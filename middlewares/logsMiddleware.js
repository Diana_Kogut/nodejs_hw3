const Log = require('../models/log');


module.exports.logs = async(req, res, next) => {
    try {
        const newLog = new Log({
            method: req.method,
            url: req.url,
            time: new Date(),
            user: req.user._id || 'notRegistered'
        });
        await newLog.save();
        next();
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
};