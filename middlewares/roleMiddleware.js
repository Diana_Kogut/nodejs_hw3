module.exports.isDriver = (req, res, next) => {
    const role = req.user.role;
    if (role === 'DRIVER') {
        next();
    } else {
        res.status(403).json({
            message: 'Forbidden'
        });
    }
};

module.exports.isShipper = (req, res, next) => {
    const role = req.user.role;
    if (role === 'SHIPPER') {
        next();
    } else {
        res.status(403).json({
            message: 'Forbidden'
        });
    }
};