const { request } = require('express');
const Joi = require('joi');

module.exports.schema = {
    registerReq: Joi.object().keys({
        email: Joi.string().email().required(),
        password: Joi.string().min(6),
        role: Joi.string().valid('DRIVER', 'SHIPPER').required()
    }),
    loginReq: Joi.object().keys({
        email: Joi.string().email().required(),
        password: Joi.string().min(6),
    }),
    forgotPassReq: Joi.object().keys({
        email: Joi.string().email().required(),
    }),
    changePassReq: Joi.object().keys({
        oldPassword: Joi.string().min(6),
        newPassword: Joi.string().min(6)
    }),
    truckReq: Joi.object().keys({
        type: Joi.string().valid('SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT')
    }),
    loadReq: Joi.object().keys({
        pickup_address: Joi.string().min(3).required(),
        delivery_address: Joi.string().min(3).required(),
        name: Joi.string().min(2).required(),
        dimensions: Joi.object().keys({
            width: Joi.number().required(),
            length: Joi.number().required(),
            height: Joi.number().required()
        }),
        payload: Joi.number().required()
    }),
    updateLoadReq: Joi.object().keys({
        pickup_address: Joi.string().min(3),
        delivery_address: Joi.string().min(3),
        name: Joi.string().min(2),
        dimensions: Joi.object().keys({
            width: Joi.number(),
            length: Joi.number(),
            height: Joi.number()
        }),
        payload: Joi.number()
    }),
};

module.exports.validateRequest = (schema) => {
    return (req, res, next) => {

        const { error } = schema.validate(req.body);
        const valid = error == null;

        if (valid) {
            next();
        } else {
            const { details } = error;
            const message = details.map(i => i.message).join(',');

            res.status(422).json({ error: message })
        }
    }
};