# nodejs_hw3

## Synopsis
Node js app to find truck drivers and assign them for load transportation.

## Installation
### 1) Make sure you have node js installed.

Open a terminal window. Type:
```
node -v
```
If installed, this will display your nodejs version.
if not, go to the link and install nodejs: https://nodejs.org

### 2) Clone the repository, install node packages
```
git clone https://gitlab.com/Diana_Kogut/nodejs_hw3
npm install
npm start
```
## API Documentation

Go to https://gitlab.com/Diana_Kogut/nodejs_hw3/-/blob/master/swagger.yaml 
