const mongoose = require('mongoose');
const Schema = mongoose.Schema;

module.exports = mongoose.model('truck', new Schema({
    created_by: {
        required: true,
        type: String,
        default: new Date()
    },
    type: {
        type: String,
        required: true,
        enum: ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT']
    },
    created_date: {
        required: true,
        type: String,
        default: new Date()
    },
    assigned_to: {
        type: String,
        default: ''
    },
    status: {
        type: String,
        enum: ['OL', 'IS']
    }
}));