const mongoose = require('mongoose');
const Schema = mongoose.Schema;

module.exports = mongoose.model('user', new Schema({
    email: {
        type: String,
        required: true,
        unique: true
    },
    created_date: {
        type: String,
        required: true,
        default: new Date()
    },
    password: {
        type: String,
        required: true
    },
    role: {
        type: String,
        required: true,
        enum: ['SHIPPER', 'DRIVER']
    }
}));