const mongoose = require('mongoose');
const Schema = mongoose.Schema;

module.exports = mongoose.model('load', new Schema({
    created_by: {
        required: true,
        type: String
    },
    pickup_address: {
        type: String,
        required: true
    },
    delivery_address: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    assigned_to: {
        type: String
    },
    created_date: {
        required: true,
        type: String,
        default: new Date()
    },
    status: {
        required: true,
        type: String,
        default: 'NEW',
        enum: ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED']
    },
    state: {
        type: String,
        enum: ['En route to Pick Up', 'Arrived to Pick Up', 'En route to delivery', 'Arrived to']
    },
    dimensions: {
        width: {
            type: Number,
            required: true
        },
        length: {
            type: Number,
            required: true
        },
        height: {
            type: Number,
            required: true
        }
    },
    payload: {
        type: Number,
        required: true
    },
    logs: [{
        message: {
            type: String,
            required: true
        },
        time: {
            type: String,
            default: new Date()
        }
    }]
}));