const mongoose = require('mongoose');
const Schema = mongoose.Schema;

module.exports = mongoose.model('log', new Schema({
    method: {
        required: true,
        type: String
    },
    url: {
        type: String,
        required: true
    },
    time: {
        required: true,
        type: String,
        default: new Date()
    },
    user: {
        type: String,
        required: true
    }
}));