const express = require('express');
const { showLogs } = require('../controllers/logController');
const router = express.Router();

router.get('/logs', showLogs);


module.exports = router;