const { request } = require('express');
const express = require('express');
const router = express.Router();

const { register, login, forgotPassword } = require('../controllers/authController');
const { schema, validateRequest } = require('../middlewares/validateRequest')

router.post('/auth/register', validateRequest(schema.registerReq), register);
router.post('/auth/login', validateRequest(schema.loginReq), login);
router.post('/auth/forgot_password', validateRequest(schema.forgotPassReq), forgotPassword);


module.exports = router;