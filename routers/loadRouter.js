const express = require('express');
const router = express.Router();
const authMiddleware = require('../middlewares/authMiddleware');
const { logs } = require('../middlewares/logsMiddleware');
const { isShipper, isDriver } = require('../middlewares/roleMiddleware');
const { schema, validateRequest } = require('../middlewares/validateRequest')

const {
    addLoad,
    updateLoad,
    deleteLoad,
    postLoad,
    shippingInfo,
    showUserLoads,
    showActiveLoads,
    showLoadById,
    interactWithDriver
} = require('../controllers/loadController');

router.get('/loads', authMiddleware, logs, showUserLoads);
router.post('/loads', validateRequest(schema.loadReq), authMiddleware, isShipper, logs, addLoad);
router.get('/loads/active', authMiddleware, isDriver, logs, showActiveLoads);
router.patch('/loads/active/state', authMiddleware, isDriver, logs, interactWithDriver);
router.get('/loads/:id/shipping_info', authMiddleware, isShipper, logs, shippingInfo);
router.get('/loads/:id', authMiddleware, isShipper, logs, showLoadById);
router.put('/loads/:id', validateRequest(schema.updateLoadReq), authMiddleware, isShipper, logs, updateLoad);
router.delete('/loads/:id', authMiddleware, isShipper, logs, deleteLoad);
router.post('/loads/:id/post', authMiddleware, isShipper, logs, postLoad);


module.exports = router;