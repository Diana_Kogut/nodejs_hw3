const express = require('express');
const router = express.Router();
const authMiddleware = require('../middlewares/authMiddleware');
const { isDriver } = require('../middlewares/roleMiddleware');
const { logs } = require('../middlewares/logsMiddleware');
const { schema, validateRequest } = require('../middlewares/validateRequest')


const {
    addTruck,
    showUserTrucks,
    assignTruck,
    updateTruck,
    deleteTruck,
    showTruckById
} = require('../controllers/truckController');

router.get('/trucks', authMiddleware, isDriver, logs, showUserTrucks);
router.get('/trucks/:id', authMiddleware, isDriver, logs, showTruckById);
router.post('/trucks', validateRequest(schema.truckReq), authMiddleware, isDriver, logs, addTruck);
router.put('/trucks/:id', validateRequest(schema.truckReq), authMiddleware, isDriver, logs, updateTruck);
router.delete('/trucks/:id', authMiddleware, isDriver, logs, deleteTruck);
router.patch('/trucks/:id/assign', authMiddleware, isDriver, logs, assignTruck);

module.exports = router;