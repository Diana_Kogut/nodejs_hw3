const express = require('express');
const router = express.Router();
const authMiddleware = require('../middlewares/authMiddleware');
const { logs } = require('../middlewares/logsMiddleware');
const { schema, validateRequest } = require('../middlewares/validateRequest')

const { showProfileInfo, changePassword, deleteProfile } = require('../controllers/userController');


router.get('/users/me', authMiddleware, logs, showProfileInfo);
router.delete('/users/me/', authMiddleware, logs, deleteProfile);
router.patch('/users/me/password', validateRequest(schema.changePassReq), authMiddleware, logs, changePassword);

module.exports = router;