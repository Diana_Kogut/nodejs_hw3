const jwt = require('jsonwebtoken');
const User = require('../models/user');
const { secret } = require('../config/auth');
const { from, service, auth } = require('../config/mail');
const Bcrypt = require('bcryptjs');
const nodemailer = require('nodemailer');


module.exports.register = async(req, res) => {
    try {
        let { email, password, role } = req.body;

        password = Bcrypt.hashSync(password, 10);

        if (!email || !password || role !== 'DRIVER' && role !== 'SHIPPER') {
            return res.status(400).json({ message: 'Enter the correct data' });
        }
        const user = new User({ email, password, role });
        await user.save();

        res.status(200).json({ message: 'Profile created successfully' });

    } catch (err) {
        res.status(500).json({ message: err.message });
    }
};

module.exports.login = async(req, res) => {

    try {
        const { email, password } = req.body;

        const currentUser = await User.findOne({ email }).exec();

        if (!currentUser) {
            res.status(400).json({ message: 'User not found' });
        }
        if (!Bcrypt.compareSync(password, currentUser.password)) {
            res.status(400).send({ message: 'The password is invalid' });
        }

        const token = jwt.sign(JSON.stringify(currentUser), secret);
        res.json({
            token,
            isDriver: currentUser.role === 'DRIVER' ? true : false
        });

    } catch (err) {
        res.status(500).json({ message: err.message });
    }
};

module.exports.forgotPassword = async(req, res) => {
    const generatedPassword = generatePassword();
    const newPassword = Bcrypt.hashSync(generatedPassword, 10);
    const { email } = req.body;

    try {
        const user = await User.findOneAndUpdate({ email }, {
            password: newPassword
        }).exec();

        sentEmail(user.email, generatedPassword, res);
    } catch (err) {
        res.status(500).json({
            message: err.message
        });
    }


};

const sentEmail = async(email, password, res) => {
    try {
        const transporter = nodemailer.createTransport({
            service,
            auth
        });

        const mailOptions = {
            from,
            to: email,
            subject: 'Forgot password',
            text: `Your new password: ${password}.
             Change it as soon as possible.`
        };

        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                console.log(error);
            } else {
                console.log('Email sent: ' + info.response);
                res.json({
                    message: 'Sented'
                });
            }
        });


    } catch (err) {
        res.status(500).json({
            message: err.message
        });
    }
};

const generatePassword = () => {
    let password = '';
    const symbols = 'ABCDEGHIJKLMNPQRSTUVWXYZabdefghijklmnopqrstuvwxyz0123456789';
    for (let i = 0; i < 10; i++) {
        password += symbols.charAt(Math.floor(Math.random() * symbols.length));
    }
    return password;
};