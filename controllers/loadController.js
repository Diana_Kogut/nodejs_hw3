const Load = require('../models/load');
const Truck = require('../models/truck');
const url = require('url');

module.exports.addLoad = async(req, res) => {

    try {
        const { name, payload, pickup_address, delivery_address, dimensions } = req.body;
        const { width, length, height } = dimensions;
        const userId = req.user._id;

        checkDimensions(payload, width, height, length, res);

        const log = {
            message: `Load created by ${req.user.email}`
        };
        const load = new Load({
            name,
            payload,
            pickup_address,
            delivery_address,
            dimensions: {
                width,
                length,
                height
            },
            logs: [{ log }],
            created_by: userId
        });

        await load.save();
        res.json({ message: 'Load created successfully' });

    } catch (err) {
        res.status(500).json({ message: err.message });
    }


};

module.exports.showUserLoads = async(req, res) => {
    try {
        const userId = req.user._id;
        const { query } = url.parse(req.url, true);
        const status = query.status;
        let findLoad;

        if (req.user.role === 'DRIVER') {
            findLoad = await Load.find({
                assigned_to: userId,
                status: status || {
                    $gte: ''
                }
            }).exec();
        } else {
            findLoad = await Load.find({
                created_by: userId,
                status: status || {
                    $gte: ''
                }
            }).exec();
        }

        if (!findLoad) res.status(400).json({ message: 'No load found' });
        res.json({ findLoad });

    } catch (err) {
        res.status(500).json({ message: err.message });
    }

};

module.exports.showLoadById = async(req, res) => {
    try {
        const loadId = req.params.id;
        const userId = req.user._id;

        const currentLoad = await Load.findOne({ _id: loadId, created_by: userId }).exec();

        if (!currentLoad) res.status(400).json({ message: 'No load found' });

        res.json({ currentLoad });
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
};

module.exports.showActiveLoads = async(req, res) => {

    try {
        const userId = req.user._id;

        const currentLoads = await Load.find({
            assigned_to: userId
        }).exec();

        if (!currentLoads) res.status(400).json({ message: 'No load found' });

        const activeLoads = currentLoads.filter((item) => item.assigned_to);

        res.json({ activeLoads });
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
};

module.exports.updateLoad = async(req, res) => {
    const loadId = req.params.id;
    const userId = req.user._id;

    if (!loadId) res.status(400).json({ message: 'No load found' });
    try {
        const currentLoad = await Load.findOne({
            _id: loadId,
            created_by: userId
        }).exec();

        const allowed = checkIfNew(currentLoad, res);

        if (allowed) {
            await Load.findOneAndUpdate({
                _id: loadId,
                created_by: userId
            }, {
                $set: req.body
            }).exec();

            await addLogs(`Load was updated on ${new Date()}`, currentLoad);
            res.json({
                message: 'Load details changed successfully'
            });
        }
    } catch (err) {
        res.status(500).json({ message: err.message });
    }

};

module.exports.deleteLoad = async(req, res) => {
    const loadId = req.params.id;
    const userId = req.user._id;

    if (!loadId) res.status(400).json({ message: 'No load found' });
    try {
        const currentLoad = await Load.findOne({
            _id: loadId,
            created_by: userId
        }).exec();

        const allowed = checkIfNew(currentLoad, res);

        if (allowed) {
            await Load.findOneAndDelete({
                _id: loadId,
                created_by: userId
            }).exec();

            res.json({
                message: 'Load deleted successfully'
            });
        }
    } catch (err) {
        res.status(500).json({ message: err.message });
    }

};

module.exports.postLoad = async(req, res) => {
    const loadId = req.params.id;
    const userId = req.user._id;

    if (!loadId) res.status(400).json({ message: 'No load found' });

    try {
        const currentLoad = await Load.findOneAndUpdate({
            _id: loadId,
            created_by: userId,
            status: 'NEW'
        }, {
            status: 'POSTED'
        }).exec();

        const { payload, dimensions } = currentLoad;
        const { width, height, length } = dimensions;

        const type = checkDimensions(payload, width, height, length, res);

        const suitableTruck = await Truck.findOne({
            status: 'IS',
            type
        }).exec();

        if (!suitableTruck) {

            await Load.findOneAndUpdate({
                _id: loadId,
                created_by: userId,
                status: 'POSTED'
            }, {
                status: 'NEW'
            }).exec();

            await addLogs(`Load  posted on ${new Date()}. Driver not found`, currentLoad);

            res.status(200).json({
                message: 'No truck available',
                driver_found: false
            });
        } else {
            await Load.findOneAndUpdate({
                _id: loadId,
                created_by: userId,
                status: 'POSTED'
            }, {
                status: 'ASSIGNED',
                state: 'En route to Pick Up',
                assigned_to: suitableTruck.assigned_to
            }).exec();

            await addLogs(`Load posted on ${new Date()}. Driver found `, currentLoad);

            res.status(200).json({
                message: 'Load posted successfully',
                driver_found: true
            });
        }

    } catch (err) {
        res.status(500).json({ message: err.message });
    }
};

module.exports.interactWithDriver = async(req, res) => {
    const loadsStates = ['En route to Pick Up', 'Arrived to Pick Up', 'En route to delivery', 'Arrived to'];
    const userId = req.user._id;

    try {
        const currentLoad = await Load.findOne({
            assigned_to: userId,
            status: 'ASSIGNED'
        });
        const currentLoadState = currentLoad.state || '';
        const index = loadsStates.indexOf(currentLoadState) || 0;
        if (index === loadsStates.length) {
            res.json({
                message: 'Already delivered'
            });
        }
        await Load.findOneAndUpdate({ _id: currentLoad.id }, {
            state: loadsStates[index + 1]
        }).exec();

        await addLogs(`Load state changed from ${currentLoadState} to ${loadsStates[index + 1]}`, currentLoad);

        res.json({
            message: `Load state changed to ${loadsStates[index + 1]}`
        });
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
};

module.exports.shippingInfo = async(req, res) => {

    const loadId = req.params.id;
    const userId = req.user._id;

    try {
        const load = await Load.findOne({ _id: loadId, created_by: userId }).exec();
        const truck = await Truck.findOne({ assigned_to: load.assigned_to }).exec();

        if (load && truck) {
            res.json({
                load,
                truck
            });
        } else {
            res.status(400).json({ message: truck ? 'load not found' : 'both or truck' });
        }
    } catch (err) {
        res.status(500).json({ message: err.message });
    }

};

const checkDimensions = (payload, width, height, length, response) => {
    if (width >= 700 || length >= 350 || height >= 200 || payload > 4000) {
        response.status(400).json({ message: 'We can not carry this load' });
        return;
    }
    if (width >= 500 || length >= 250 || height >= 170 || payload >= 2500) {
        return 'LARGE STRAIGHT';
    }
    if (width >= 300 || length >= 250 || height >= 170 || payload >= 1700) {
        return 'SMALL STRAIGHT';
    }
    return 'SPRINTER';
};

const addLogs = async(message, loadId) => {
    try {
        const currentLoad = await Load.findOne({ _id: loadId }).exec();

        await Load.findOneAndUpdate({ _id: loadId }, {
            logs: [...currentLoad.logs, message]
        });
    } catch (err) {
        response.status(500).json({ message: err.message });
    }
};

const checkIfNew = (load, res) => {
    if (load.status !== 'NEW') {
        res.status(400).json({ message: 'Forbiddent. You can change only load with status NEW' });
        return false;
    }
    return true;
};