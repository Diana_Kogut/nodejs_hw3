const User = require('../models/user');
const jwt = require('jsonwebtoken');
const { secret } = require('../config/auth');

module.exports.showProfileInfo = async(req, res) => {
    try {
        const userId = req.user._id;
        const currentUser = await User.findOne({ _id: userId }).exec();

        res.status(200).json({
            _id: userId,
            email: currentUser.email,
            role: currentUser.role,
            created_date: currentUser.created_date
        });

    } catch (err) {
        res.status(500).json({ message: err.message });
    }
};


module.exports.deleteProfile = async(req, res) => {
    try {
        const userId = req.user._id;

        const currentUser = await User.findOneAndDelete({ _id: userId }).exec();

        if (!currentUser) res.status(400).json({ message: 'User not found' });

        res.json({ message: 'Profile deleted successfully' });

    } catch (err) {
        res.status(500).json({ message: err.message });
    }


};


module.exports.changePassword = async(req, res) => {

    try {
        const email = req.user.email;
        const { oldPassword, newPassword } = req.body;

        const currentUser = await User.findOne({ email }).exec();

        if (currentUser.password !== oldPassword) {
            return res.status(400).json({ message: 'You entered incorrect old password' });
        }

        await User.findOneAndUpdate({ email }, { password: newPassword }).exec();

        const token = jwt.sign(JSON.stringify(currentUser), secret);
        res.json({ message: 'Password changed successfully', token });

    } catch (err) {
        res.status(500).json({ message: err.message });
    }
};