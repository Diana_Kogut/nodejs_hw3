const Log = require('../models/log');

module.exports.showLogs = async(req, res) => {
    try {
        const logs = await Log.find();
        res.json({ logs });

    } catch (err) {
        res.status(500).json({ message: err.message });
    }
};