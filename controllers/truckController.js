const Truck = require('../models/truck');

module.exports.addTruck = async(req, res) => {

    try {
        const { type } = req.body;
        if (!type) res.status(400).json({ message: 'Enter correct data' });

        const truck = new Truck({ created_by: req.user._id, type, created_date: new Date() });
        await truck.save();

        res.json({ message: 'Truck created successfully' });
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
};

module.exports.showUserTrucks = async(req, res) => {
    try {
        const created_by = req.user._id;

        const currentTrucks = await Truck.find({
            created_by
        }).exec();

        if (!currentTrucks) res.status(400).json({ message: 'No trucks found' });
        res.json({ currentTrucks });

    } catch (err) {
        res.status(500).json({ message: err.message });
    }


};

module.exports.showTruckById = async(req, res) => {

    try {
        const truckId = req.params.id;
        const userId = req.user._id;

        if (!truckId) res.status(400).json({ message: 'No truck found' });

        const currentTruck = await Truck.findOne({ _id: truckId, created_by: userId }).exec();
        res.json(currentTruck);

    } catch (err) {
        res.status(500).json({ message: err.message });
    }
};

module.exports.updateTruck = async(req, res) => {
    const truckId = req.params.id;
    const userId = req.user._id;

    if (!truckId) res.status(400).json({ message: 'No truck found' });

    try {
        const currentTruck = await Truck.findOne({ _id: truckId, created_by: userId }).exec();

        if (!currentTruck) res.status(400).json({ message: 'Truck not found' });

        const allowed = checkIfAssigned(currentTruck);
        if (allowed) {
            await Truck.findOneAndUpdate({
                _id: truckId,
                created_by: userId
            }, {
                type: req.body.type
            }).exec();

            res.json({ message: 'Truck details changed successfully' });
        }

    } catch (err) {
        res.status(500).json({ message: err.message });
    }
};

module.exports.deleteTruck = async(req, res) => {
    const truckId = req.params.id;
    const userId = req.user._id;

    if (!truckId) res.status(400).json({ message: 'No truck found' });

    try {
        const currentTruck = await Truck.findOne({ _id: truckId, created_by: userId }).exec();

        if (!currentTruck) res.status(400).json({ message: 'Truck not found' });

        const allowed = checkIfAssigned(currentTruck);
        if (allowed) {
            await Truck.findOneAndDelete({
                _id: truckId,
                created_by: userId
            }).exec();

            res.json({ message: 'Truck deleted successfully' });
        }
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
};

module.exports.assignTruck = async(req, res) => {
    try {
        const truckId = req.params.id;
        const userId = req.user._id;
        const forbidden = await Truck.findOne({ assigned_to: userId }).exec();

        if (forbidden) {
            res.status(400).json({ message: 'Forbidden. You already have assigned truck' });
        } else {
            await Truck.findOneAndUpdate({ _id: truckId }, {
                assigned_to: userId,
                status: 'IS'
            }).exec();

            res.json({ message: 'Truck assigned successfully' });
        }
    } catch (err) {
        res.status(500).json({ message: err.message });
    }

};

const checkIfAssigned = (truck, res) => {
    if (truck.assigned_to) {
        res.status(400).json({ message: 'Forbiddent. You cannot change the assigned truck' });
        return false;
    }
    return true;
};