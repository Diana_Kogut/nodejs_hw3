require('dotenv').config();

module.exports = {
    from: process.env.MAIL_FROM,
    service: process.env.MAIL_SERVICE,
    auth: {
        user: process.env.MAIL_FROM,
        pass: process.env.MAIL_PASS
    }
};