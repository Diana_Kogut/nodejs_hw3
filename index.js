const express = require('express');
const mongoose = require('mongoose');
const cookieParser = require('cookie-parser');

const cors = require('cors');

const { port } = require('./config/server');
const { dbUrl } = require('./config/database');
const { secret } = require('./config/auth');

const app = express();

const authRouter = require('./routers/authRouter');
const userRouter = require('./routers/userRouter');
const loadRouter = require('./routers/loadRouter');
const truckRouter = require('./routers/truckRouter');
const logRouter = require('./routers/logRouter');


mongoose.connect(dbUrl, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true
});

app.use(cors());
app.use(express.json());
app.use(cookieParser(secret));

app.use('/api', authRouter);
app.use('/api', truckRouter);
app.use('/api', userRouter);
app.use('/api', loadRouter);
app.use('/api', logRouter);


app.listen(port, () => console.log(`Server started on ${port} port`));